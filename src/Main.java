import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.highcalibrewizard.simpleengine.CanvasText;
import com.highcalibrewizard.simpleengine.DisplayFacade;
import com.highcalibrewizard.simpleengine.Entity;
import com.highcalibrewizard.simpleengine.Game;
import com.highcalibrewizard.simpleengine.GameCanvas;
import com.highcalibrewizard.simpleengine.ImageLoader;
import com.highcalibrewizard.simpleengine.Tick;

public class Main implements Tick {
	
	Game game;
	GameCanvas canvas;
	DisplayFacade display;
	BufferedImage mouseImage;
	List<Entity> mouses;
	CanvasText timer;
	long startTime;
	long finalTime;
	boolean play = true;

	public static void main(String[] args) {
		new Main();

	}
	
	public Main() {
		display = DisplayFacade.getInstance();
		game = new Game(new Dimension(display.getWidth(), display.getHeight()), "Clickergame", this, Color.WHITE, true);
		canvas = game.getCanvas();
		
		mouseImage = ImageLoader.getImage("mouse.png");
		
		mouses = new ArrayList<>();
		for(int i = 0; i < 10; i++) {
			int x = (int)(Math.random()*(display.getWidth()-100));
			int y = (int)(Math.random()*(display.getHeight()-100));
			mouses.add(new Entity(new Rectangle(x,y,100,100), mouseImage));
		}
		
		timer = new CanvasText("", 0, 32, new Font(null, Font.PLAIN, 32));
		startTime = System.currentTimeMillis();
		
		game.init();
	}

	@Override
	public void keyPressed(int key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClick(MouseEvent e) {
		Iterator<Entity> iterator = mouses.iterator();
		Point p = e.getPoint();
		
		while(iterator.hasNext()) {
			Entity ent = iterator.next();
			if (ent.detectCollision(p)) {
				iterator.remove();
				break;
			}
			
		}
		
	}

	@Override
	public void mousePosition(Point p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tick(Graphics g) {
		canvas.clear(g);
		
		Iterator<Entity> iterator = mouses.iterator();
		
		while(iterator.hasNext()) {
			Entity ent = iterator.next();
			canvas.drawEntity(ent, g);
		}
		
		if (mouses.size() == 0 && play) {
			play = false;
			long endTime = System.currentTimeMillis();
			finalTime = (endTime - startTime) / 1000;
			timer.setText("Your final time is: " + finalTime);
		}
		
		if (!play) {
			canvas.drawText(timer, g);
		}
		
	}

}
